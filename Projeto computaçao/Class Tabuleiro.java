package projeto;

public class Tabuleiro
{
	
	String [][] matrix = new String[9][9];

	
	public boolean TestePosicoesLivres(int linha, int coluna, int posicao, int tamanho){

		if(posicao == 0){
			for(int i = 0; i<tamanho; i++)
				if(!matrix[linha + i][coluna].equals(" "))
					return false;
		}

		if(posicao == 1){
			for(int i = 0; i<tamanho; i++)
				if(!matrix[linha][coluna+i].equals(" "))
					return false;
		}

		return true;

	}

	public void colocaBarco(int linha, int coluna, int posicao, int tamanho, String letra){

		
		if(posicao == 0)
			for(int i = 0; i<tamanho; i++)
				alteraposicao(linha+i, coluna, letra);
				
		

		
		if(posicao == 1)
			for(int i = 0; i<tamanho; i++)
				alteraposicao(linha, coluna+i, letra);

	}
	
	public void CriaMinhaMatriz()
	{
		String[] linha = {" ","A","B","C","D","E","F","G","H"};
		String[] coluna1 = {" ","1","2","3","4","5","6","7","8"};
		
		for(int a =1; a<9;a++)
		{
			for(int b = 1; b<9; b++)
			{
				matrix[a][b] = " ";
			}
		}
		for(int a = 0; a<9; a++)
		{
			matrix[a][0]=linha[a];
			matrix[0][a]= coluna1[a];
		} 
	}
	
	public void criamatrizadversaria()
	{
		String[] linha = {" ","A","B","C","D","E","F","G","H"};
		String[] coluna1 = {" ","1","2","3","4","5","6","7","8"};
		for(int a =1; a<9;a++)
		{
			for(int b = 1; b<9; b++)
			{
				matrix[a][b] = "?";
			}
		}
		
	
		for(int a = 0; a<9; a++)
		{
			matrix[a][0]=linha[a];
			matrix[0][a]= coluna1[a];
		}
		
	}
	
	public void alteraposicao(int linha, int coluna , String peca)
	{		
		matrix [linha][coluna] =  peca;
		
		
		
	}
	
	public String verificaposicao(int linha, int coluna)
	{
		return matrix[linha][coluna];
		
	}
	
	public void ImprimeMatriz ()
	{
		for(int a = 0; a<9;a++)
		{
			for(int b = 0; b<9; b++)
			{
				if(matrix[a][b].equals("D1") || matrix[a][b].equals("D2"))
					System.out.print("D ");
				else
					System.out.print(matrix[a][b] + " ");
			}
			System.out.println("");
		}
	}
	
	
}