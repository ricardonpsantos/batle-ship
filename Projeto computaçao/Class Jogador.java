package projeto;


import java.util.*;


public class Jogador {
	
	String nome;
	Tabuleiro meuTabuleiro = new Tabuleiro();
	Tabuleiro adversarioTabuleiro = new Tabuleiro();
	int[] barcos = {5,4,3,2,2};
	int tirosEfectuados = 0;
	
	
	int[] atingido = new int[5];
	Scanner s = new Scanner(System.in);
	

	public void alteraNome(String nome1) {
		nome = nome1;
	}

	
	public String Verificaataque (int linha, int coluna)
	{
		String[] barco = {"A","B","C","D1","D2"};
		String acerto = "X ";
		String aolado = "M ";

		String posicao = meuTabuleiro.verificaposicao(linha, coluna);

		
		if(posicao.equals("A"))
			barcos[0]--;

		if(posicao.equals("B"))
			barcos[1]--;

		if(posicao.equals("C"))
			barcos[2]--;

		if(posicao.equals("D1"))
			barcos[3]--;

		if(posicao.equals("D2"))
			barcos[4]--;

		if(posicao.equals(" ")){
			meuTabuleiro.alteraposicao(linha, coluna, "M");
			return "M";
		}
		else {
			meuTabuleiro.alteraposicao(linha, coluna, "X");
			return "X";
		}
	}
	
	
	public int[] ataqueManual()
	{
		Scanner s = new Scanner(System.in);
		System.out.println("introduza linha e coluna para atacar: ");
		String linhatoconvert =  s.next();						
		int coluna = s.nextInt();
		int linha = Jogo.Convert(linhatoconvert);
		int[] ataque = {linha, coluna};
		tirosEfectuados++;
		return ataque;
		
	
	}

	
	public int[] ataqueAutomaticoNivel1()
	{
		Random r = new Random();
		int a = r.nextInt(8)+1;
		int b = r.nextInt(8)+1;
		while(!adversarioTabuleiro.verificaposicao(a,b).equals("?")){
			a = r.nextInt(8)+1;
			b = r.nextInt(8)+1;
		}

		int[] ataque = {a, b};
		tirosEfectuados++;
		return ataque;			
	}

	
	 
	public boolean EscolherPecasManual (int linha, int coluna, String letra, int tamanho, int posicao)  
	{

		if(posicao == 0){
			if(linha + tamanho > 8)
				return false;
			if(!meuTabuleiro.TestePosicoesLivres(linha, coluna, posicao, tamanho))
				return false;
		}

		
		if(posicao == 1){
			if(coluna + tamanho > 8)
				return false;
			if(!meuTabuleiro.TestePosicoesLivres(linha, coluna, posicao, tamanho))
				return false;
		}

		meuTabuleiro.colocaBarco(linha, coluna, posicao, tamanho, letra);
		return true;
	}

			
	public void EscolherPecasAutomatico (String letra, int tamanhobarco)  
	{

		Random r = new Random ();
		int posicao = r.nextInt(2); //0 vertical; 1 horizontal
		int linha = 0;
		int coluna = 0;

		
		if(posicao == 0){
			linha = r.nextInt(8 - tamanhobarco) + 1;
			coluna = r.nextInt(8) + 1;
			while(!meuTabuleiro.TestePosicoesLivres(linha, coluna, posicao, tamanhobarco)){
				linha = r.nextInt(8 - tamanhobarco) + 1;
				coluna = r.nextInt(8) + 1;
			}
		}

		
		if(posicao == 1){
			linha = r.nextInt(8) + 1;
			coluna = r.nextInt(8 - tamanhobarco) + 1;
			while(!meuTabuleiro.TestePosicoesLivres(linha, coluna, posicao, tamanhobarco)){
				linha = r.nextInt(8) + 1;
				coluna = r.nextInt(8 - tamanhobarco) + 1;
			}
		}

		meuTabuleiro.colocaBarco(linha, coluna, posicao, tamanhobarco, letra);
	}
	
	public int QuantosBarcos ()
	{
		int contador = 0;
		for(int a = 0; a < 5; a++)
		{
			if( barcos[a]!= 0)
			{
				contador++;
			}
		}
		
		return contador;

		
	}
	public int TirosRecebidos ()

	{
		int contagem = 0;
		for(int a = 0; a<5; a++)
		{
			contagem += barcos[a];
		}
		
		int diferenca = 16 - contagem;
		return diferenca;
	}
	
	public static int Convert1(String a)
	{

		String [] letras = new String[8];

		letras[0] = "a";
		letras[1] = "b";
		letras[2] = "c";
		letras[3] = "d";
		letras[4] = "e";
		letras[5] = "f";
		letras[6] = "g";
		letras[7] = "h";
		
		for( int b = 0; b<8;b++)
		{
			if(a.equals(letras[b]))
			{
				return b+1;
				
			}			
			
		}

		System.out.println("Letra introduzida nao reconhecida!");
		
		return -1;
	}
	
}
	