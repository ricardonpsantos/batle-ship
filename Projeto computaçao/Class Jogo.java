package projeto;

import java.util.*;

public class Jogo {

	public static void main(String[] args) {

		ListaLigada listaJogadas = new ListaLigada();

		Jogador jogador1 = new Jogador();
		Jogador jogador2 = new Jogador();

		int[] listaLinhas = new int[128];
		int[] listaColunas = new int[128];
		String[] listaresultado = new String[128];
		int numerojogada = 0;

		Scanner s = new Scanner(System.in);
		System.out.println("Bem vindo ao jogo da batalha naval \n Que tipo de jogo pretende\n 1- Humano vs Computador \n 2- Humano vs Humano \n 3- Computador vs Computador ");
		int escolha = s.nextInt();
		int escolha1;
		Random r = new Random();

		jogador1.meuTabuleiro.CriaMinhaMatriz();
		jogador1.adversarioTabuleiro.criamatrizadversaria();
		
		jogador2.meuTabuleiro.CriaMinhaMatriz();
		jogador2.adversarioTabuleiro.criamatrizadversaria();
	
			if (escolha == 1) 
			{
				System.out.println("Jogador 1: Introduza o seu nome:");
				s.nextLine(); 
				String nome = s.nextLine();
				jogador1.alteraNome(nome);
	
				
				System.out.println("Como deseja as pecas: \n 1-Manuais \n 2-Automaticas");
				escolha1 = s.nextInt();

									
					if(escolha1 == 1)
					{						
						colocaManual(jogador1);
					
					}
					else if (escolha1 == 2) {			
									
						colocaAutomatico(jogador1);
					}

			jogador2.alteraNome("computador1");
			System.out.println("preencher o tabuleiro do computador 2...");
			colocaAutomatico(jogador2);
		}
		

		else if  (escolha == 2) 
		{
					System.out.println("Jogador 1: Introduza o seu nome:");
					s.nextLine(); 
					String nome = s.nextLine();
					jogador1.alteraNome(nome);
		
					
					System.out.println("Como deseja as pecas: \n 1-Manuais \n 2-Automaticas");
					escolha1 = s.nextInt();
						if(escolha1 == 1) 
						{
							
							colocaManual(jogador1);
			
						}
						else if (escolha1 == 2) 
						{
							
							colocaAutomatico(jogador1);
						}
		
					System.out.println("Jogador 2: Introduza o seu nome:");
					s.nextLine();
					nome = s.nextLine();
					jogador2.alteraNome(nome);

			
					System.out.println("Como deseja as pecas: \n 1-Manuais \n 2-Automaticas");
					escolha1 = s.nextInt();
					if(escolha1 == 1) 
					{
						jogador2.meuTabuleiro.CriaMinhaMatriz();
						colocaManual(jogador2);
		
					}
					else if (escolha1 == 2)
					{
						jogador2.meuTabuleiro.CriaMinhaMatriz();
						colocaAutomatico(jogador2);
		
					}

		}

		else if (escolha == 3)
		{
			jogador1.alteraNome("computador1");
			System.out.println("preencher o tabuleiro do computador 1...");
			colocaAutomatico(jogador1);

			jogador2.alteraNome("computador2");
			System.out.println("preencher o tabuleiro do computador 2...");
			colocaAutomatico(jogador2);

		}

		

		
		int[] jogada;
		String verificaResultado;

		boolean jogador1Venceu = false;
		boolean jogador2Venceu = false;


		if(escolha == 1){
			while(!jogador1Venceu && !jogador2Venceu)
			{
				
				mostraestado(jogador1, jogador2);
				System.out.println("Introduza ataque:");
				jogada = jogador1.ataqueManual();


				

				verificaResultado = jogador2.Verificaataque(jogada[0], jogada[1]);



				listaLinhas[numerojogada] = jogada[0];
				listaColunas[numerojogada] = jogada[1];
				listaresultado[numerojogada] = verificaResultado;
				numerojogada++;


				jogador1.adversarioTabuleiro.alteraposicao(jogada[0], jogada[1], verificaResultado);
				System.out.println("Resultado da jogada do " + jogador1.nome + ": "  + verificaResultado);

				if(jogador2.QuantosBarcos()==0){
					jogador1Venceu = true;
					break;
				}

			
				System.out.println("Turno computador...");
				jogada = jogador2.ataqueAutomaticoNivel1();
				verificaResultado = jogador1.Verificaataque(jogada[0], jogada[1]);

				listaLinhas[numerojogada] = jogada[0];
				listaColunas[numerojogada] = jogada[1];
				listaresultado[numerojogada] = verificaResultado;
				numerojogada++;

				jogador2.adversarioTabuleiro.alteraposicao(jogada[0], jogada[1], verificaResultado);
				System.out.println("Resultado da jogada do " + jogador2.nome + ": "  + verificaResultado);

				if(jogador1.QuantosBarcos()==0){
					jogador2Venceu = true;
					break;
				}
			}
		}		

		
		if(escolha == 2){
			while(!jogador1Venceu && !jogador2Venceu)
			{
				
				mostraestado(jogador1, jogador2);
				System.out.println("Introduza ataque:");
				jogada = jogador1.ataqueManual();
				verificaResultado = jogador2.Verificaataque(jogada[0], jogada[1]);

				listaLinhas[numerojogada] = jogada[0];
				listaColunas[numerojogada] = jogada[1];
				listaresultado[numerojogada] = verificaResultado;
				numerojogada++;
							
				jogador1.adversarioTabuleiro.alteraposicao(jogada[0], jogada[1], verificaResultado);
				System.out.println("Resultado da jogada do " + jogador1.nome + ": "  + verificaResultado);

				if(jogador2.QuantosBarcos()==0){
					jogador1Venceu = true;
					break;
				}

				
				mostraestado(jogador2, jogador1);
				jogada = jogador2.ataqueManual();
				verificaResultado = jogador1.Verificaataque(jogada[0], jogada[1]);

				listaLinhas[numerojogada] = jogada[0];
				listaColunas[numerojogada] = jogada[1];
				listaresultado[numerojogada] = verificaResultado;
				numerojogada++;

				jogador2.adversarioTabuleiro.alteraposicao(jogada[0], jogada[1], verificaResultado);
				System.out.println("Resultado da jogada do " + jogador2.nome + ": "  + verificaResultado);

				if(jogador1.QuantosBarcos()==0){
					jogador2Venceu = true;
					break;
				}
			}
		}

		
		if(escolha == 3){
			while(!jogador1Venceu && !jogador2Venceu)
			{
			
				System.out.println("Turno computador 1...");
				jogada = jogador1.ataqueAutomaticoNivel1();
				verificaResultado = jogador2.Verificaataque(jogada[0], jogada[1]);

				listaLinhas[numerojogada] = jogada[0];
				listaColunas[numerojogada] = jogada[1];
				listaresultado[numerojogada] = verificaResultado;
				numerojogada++;

				jogador1.adversarioTabuleiro.alteraposicao(jogada[0], jogada[1], verificaResultado);
				System.out.println("Resultado da jogada do " + jogador1.nome + ": "  + verificaResultado);

				if(jogador2.QuantosBarcos()==0){
					jogador1Venceu = true;
					break;
				}

				
				System.out.println("Turno computador 2...");
				jogada = jogador2.ataqueAutomaticoNivel1();
				verificaResultado = jogador1.Verificaataque(jogada[0], jogada[1]);

				listaLinhas[numerojogada] = jogada[0];
				listaColunas[numerojogada] = jogada[1];
				listaresultado[numerojogada] = verificaResultado;
				numerojogada++;

				jogador2.adversarioTabuleiro.alteraposicao(jogada[0], jogada[1], verificaResultado);
				System.out.println("Resultado da jogada do " + jogador2.nome + ": "  + verificaResultado);

				if(jogador1.QuantosBarcos()==0){
					jogador2Venceu = true;
					break;
				}
			}
		}



		System.out.println("Jogo Terminado!");
		if(jogador1Venceu == true)
			System.out.println("Jogador 1 venceu!");
		if(jogador2Venceu == true)
			System.out.println("Jogador 2 venceu!");


		for(int i =0; i<numerojogada; i++)
			System.out.println("Jogador " + (i%2+1) +": Jogada Nro: " + i + " Linha: " + listaLinhas[i] + " Coluna: " + listaColunas[i] + " Resultado: " +  listaresultado[i]);

	}

	public static void colocaManual(Jogador j) {
		Scanner s = new Scanner(System.in);
		boolean sucesso = false;
		String[] barcos = {"aircraft", "batleshiip", "Cruiser", "Destroyer 1", "Destroyer 2"};
		int[] tamanho = {5,4,3,2,2};
		String [] caracter = {"A","B","C","D1","D2"};
		int posicao = 0;
		int coluna = 0;
		int linha = 0;

		for(int a = 0 ; a < 5;  a++)
		{
			System.out.println("Posi�aoo do barco \n 1- Vertical \n 2- Horizontal" );
			posicao = s.nextInt();
			System.out.println("Introduza linha e coluna para o "+ barcos[a]);
			String linhatoconvert =  s.next();							
			coluna = s.nextInt();
			linha = Convert(linhatoconvert);
			sucesso = j.EscolherPecasManual(linha, coluna, caracter[a], tamanho[a], posicao);
			
			while(!sucesso){
				System.out.println("Posicao invalida! Introduza novamente!");
				System.out.println("Posi�ao do barco \n 1- Vertical \n 2- Horizontal" );
				posicao = s.nextInt();
				System.out.println("introduza linha e coluna para o "+ barcos[a]);
				linhatoconvert = s.next();							
				coluna = s.nextInt();
				linha = Convert(linhatoconvert);
				sucesso = j.EscolherPecasManual(linha, coluna, caracter[a], tamanho[a], posicao);
			}			
		}
	}

	public static void colocaAutomatico(Jogador j) {
		Random r = new Random();
		for(int a = 0 ; a < 5 ;  a++)
		{
			int[] tamanho = {5,4,3,2,2};
			String [] caracter = {"A","B","C","D1","D2"};
			j.EscolherPecasAutomatico(caracter[a], tamanho[a]);	
							
		}
	}

	public static void mostraestado(Jogador jogadoractual, Jogador adversario) {
		System.out.println("Turno JOGADOR " + jogadoractual.nome);

		System.out.println("## Enemy Board ##");
		jogadoractual.adversarioTabuleiro.ImprimeMatriz();

		System.out.println("## My Board ##");
		jogadoractual.meuTabuleiro.ImprimeMatriz();

		System.out.println("My ships " + jogadoractual.QuantosBarcos()+ "/5");
		System.out.println("Enemy ships " + adversario.QuantosBarcos()+ "/5");

		System.out.println("My shots " + adversario.TirosRecebidos() + "/" + jogadoractual.tirosEfectuados);
		System.out.println("Enemy successfull shots " + jogadoractual.TirosRecebidos() + "/" + adversario.tirosEfectuados);		
	}

	public static int Convert(String a)
	{

		String [] letras = new String[9];

		letras[1] = "A";
		letras[2] = "B";
		letras[3] = "C";
		letras[4] = "D";
		letras[5] = "E";
		letras[6] = "F";
		letras[7] = "G";
		letras[8] = "H";
		
		for( int b = 0; b<9;b++)
		{
			if(a.toUpperCase().equals(letras[b]))
			{
				return b;
			}			
		}

		System.out.println("Letra introduzida nao reconhecida!");
		
		return -1;
		
	}
}




